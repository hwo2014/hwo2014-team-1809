package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class Car {

    private CarId id;

    private CarDimensions dimensions;

    public Car() {
    }

    public Car(CarId id, CarDimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }

    public CarId getId() {
        return id;
    }

    public void setId(CarId id) {
        this.id = id;
    }

    public CarDimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(CarDimensions dimensions) {
        this.dimensions = dimensions;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, dimensions);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Car) {
            Car other = (Car) obj;
            return equal(id, other.id)
                    && equal(dimensions, other.dimensions);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("dimensions", dimensions)
                .toString();
    }
}
