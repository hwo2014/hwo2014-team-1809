package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class CarDimensions {

    private Double length;

    private Double width;

    private Double guideFlagPosition;

    public CarDimensions() {
    }

    public CarDimensions(Double length, Double width, Double guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getGuideFlagPosition() {
        return guideFlagPosition;
    }

    public void setGuideFlagPosition(Double guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(length, width, guideFlagPosition);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CarDimensions) {
            CarDimensions other = (CarDimensions) obj;
            return equal(length, other.length)
                    && equal(width, other.width)
                    && equal(guideFlagPosition, other.guideFlagPosition);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("length", length)
                .add("width", width)
                .add("guideFlagPosition", guideFlagPosition)
                .toString();
    }
}
