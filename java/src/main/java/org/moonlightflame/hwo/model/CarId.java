package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class CarId {

    @SerializedName("name")
    private String driverName;

    private String color;

    public CarId() {
    }

    public CarId(String driverName, String color) {
        this.driverName = driverName;
        this.color = color;
    }

    public String getDriverName() {
        return driverName;
    }

    public CarId setDriverName(String driverName) {
        this.driverName = driverName;
        return this;
    }

    public String getColor() {
        return color;
    }

    public CarId setColor(String color) {
        this.color = color;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(driverName, color);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CarId) {
            CarId other = (CarId) obj;
            return equal(driverName, other.driverName) 
                    && equal(color, other.color);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("driverName", driverName)
                .add("color", color)
                .toString();
    }
}
