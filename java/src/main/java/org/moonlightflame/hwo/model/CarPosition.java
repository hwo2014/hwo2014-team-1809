package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class CarPosition {

    private CarId id;

    private Double angle;

    private PiecePosition piecePosition;

    public CarPosition() {
    }

    public CarPosition(CarId id, Double angle, PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    public CarId getId() {
        return id;
    }

    public void setId(CarId id) {
        this.id = id;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, angle, piecePosition);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CarPosition) {
            CarPosition other = (CarPosition) obj;
            return equal(id, other.id)
                    && equal(angle, other.angle)
                    && equal(piecePosition, other.piecePosition);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("angle", angle)
                .add("piecePosition", piecePosition)
                .toString();
    }
}
