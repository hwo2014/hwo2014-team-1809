package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class Driver {

    private String name;

    private String key;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, key);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Driver) {
            Driver other = (Driver) obj;
            return equal(name, other.name)
                    && equal(key, other.key);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("name", name)
                .add("key", key)
                .toString();
    }
}
