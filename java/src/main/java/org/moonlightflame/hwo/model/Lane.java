package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class Lane {

    private Double distanceFromCenter;

    private Integer index;


    public Lane() {
    }

    public Lane(Integer index, Double distanceFromCenter) {
        this.index = index;
        this.distanceFromCenter = distanceFromCenter;
    }

    public Double getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public Lane setDistanceFromCenter(Double distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
        return this;
    }

    public Integer getIndex() {
        return index;
    }

    public Lane setIndex(Integer index) {
        this.index = index;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(distanceFromCenter, index);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Lane) {
            Lane other = (Lane) obj;
            return equal(distanceFromCenter, other.distanceFromCenter)
                    && equal(index, other.index);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("distanceFromCenter", distanceFromCenter)
                .add("index", index)
                .toString();
    }
}
