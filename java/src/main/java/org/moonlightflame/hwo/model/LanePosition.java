package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class LanePosition {

    private Integer startLaneIndex;

    private Integer endLaneIndex;

    public LanePosition() {
    }

    public LanePosition(Integer startLaneIndex, Integer endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }

    public Integer getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(Integer startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    public Integer getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(Integer endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(startLaneIndex, endLaneIndex);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LanePosition) {
            LanePosition other = (LanePosition) obj;
            return equal(startLaneIndex, other.startLaneIndex)
                    && equal(endLaneIndex, other.endLaneIndex);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("startLaneIndex", startLaneIndex)
                .add("endLaneIndex", endLaneIndex)
                .toString();
    }
}
