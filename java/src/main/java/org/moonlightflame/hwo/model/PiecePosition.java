package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class PiecePosition {

    private Integer pieceIndex;

    private Double inPieceDistance;

    private LanePosition lane;

    private Integer lap;

    public PiecePosition() {
    }

    public PiecePosition(Integer pieceIndex, Double inPieceDistance, LanePosition lane, Integer lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }

    public Integer getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(Integer pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public Double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(Double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public LanePosition getLane() {
        return lane;
    }

    public void setLane(LanePosition lane) {
        this.lane = lane;
    }

    public Integer getLap() {
        return lap;
    }

    public void setLap(Integer lap) {
        this.lap = lap;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(pieceIndex, inPieceDistance, lane, lap);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PiecePosition) {
            PiecePosition other = (PiecePosition) obj;
            return equal(pieceIndex, other.pieceIndex)
                    && equal(inPieceDistance, other.inPieceDistance)
                    && equal(lane, other.lane)
                    && equal(lap, other.lap);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("pieceIndex", pieceIndex)
                .add("inPieceDistance", inPieceDistance)
                .add("lane", lane)
                .add("lap", lap)
                .toString();
    }
}
