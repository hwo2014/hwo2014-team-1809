package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import java.util.List;


/**
 * @author aectann
 */
public class Race {

    private Track track;

    private List<Car> cars;

    private RaceSession raceSession;

    public Race() {
    }

    public Race(Track track, List<Car> cars, RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(track, cars, raceSession);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Race) {
            Race other = (Race) obj;
            return Objects.equal(track, other.track)
                    && Objects.equal(cars, other.cars)
                    && Objects.equal(raceSession, other.raceSession);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("track", track)
                .add("cars", cars)
                .add("raceSession", raceSession)
                .toString();
    }
}
