package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class RaceData {

    private Race race;

    public RaceData() {
    }

    public RaceData(Race race) {
        this.race = race;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(race);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RaceData) {
            RaceData other = (RaceData) obj;
            return equal(race, other.race);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("race", race)
                .toString();
    }
}
