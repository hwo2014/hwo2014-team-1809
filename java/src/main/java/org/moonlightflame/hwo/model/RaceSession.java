package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class RaceSession {

    private Integer laps;

    private Long maxLapTimeMs;

    private Boolean quickRace;

    public RaceSession() {
    }

    public RaceSession(Integer laps, Long maxLapTimeMs, Boolean quickRace) {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }

    public Integer getLaps() {
        return laps;
    }

    public void setLaps(Integer laps) {
        this.laps = laps;
    }

    public Long getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(Long maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public Boolean getQuickRace() {
        return quickRace;
    }

    public void setQuickRace(Boolean quickRace) {
        this.quickRace = quickRace;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(laps, maxLapTimeMs, quickRace);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RaceSession) {
            RaceSession other = (RaceSession) obj;
            return equal(laps, other.laps)
                    && equal(maxLapTimeMs, other.maxLapTimeMs)
                    && equal(quickRace, other.quickRace);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("laps", laps)
                .add("maxLapTimeMs", maxLapTimeMs)
                .add("quickRace", quickRace)
                .toString();
    }
}
