package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class StartPosition {

    private Coordinates position;

    private Double angle = 0d;


    public Coordinates getPosition() {
        return position;
    }

    public StartPosition setPosition(Coordinates position) {
        this.position = position;
        return this;
    }

    public Double getAngle() {
        return angle;
    }

    public StartPosition setAngle(Double angle) {
        this.angle = angle;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(position, angle);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StartPosition) {
            StartPosition other = (StartPosition) obj;
            return equal(position, other.position)
                    && equal(angle, other.angle);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("position", position)
                .add("angle", angle)
                .toString();
    }
}
