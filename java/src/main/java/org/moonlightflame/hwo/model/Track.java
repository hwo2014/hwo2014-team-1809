package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;

import java.util.List;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class Track {

    private String id;

    private String name;

    private List<TrackPiece> pieces;

    private List<Lane> lanes;

    private StartPosition startingPoint;

    public Track() {
    }

    public Track(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Track setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Track setName(String name) {
        this.name = name;
        return this;
    }

    public List<TrackPiece> getPieces() {
        return pieces;
    }

    public Track setPieces(List<TrackPiece> pieces) {
        this.pieces = pieces;
        return this;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public Track setLanes(List<Lane> lanes) {
        this.lanes = lanes;
        return this;
    }

    public StartPosition getStartingPoint() {
        return startingPoint;
    }

    public Track setStartingPoint(StartPosition startingPoint) {
        this.startingPoint = startingPoint;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, pieces, lanes, startingPoint);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Track) {
            Track other = (Track) obj;
            return equal(id, other.id)
                    && equal(name, other.name)
                    && equal(pieces, other.pieces)
                    && equal(lanes, other.lanes)
                    && equal(startingPoint, other.startingPoint);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("pieces", pieces)
                .add("lanes", lanes)
                .add("startingPoint", startingPoint)
                .toString();
    }
}
