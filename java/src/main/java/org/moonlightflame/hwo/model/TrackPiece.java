package org.moonlightflame.hwo.model;


import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class TrackPiece {

    private Double length;

    @SerializedName("switch")
    private boolean hasSwitch;

    private Double radius;

    private Double angle;

    public Double getLength() {
        return length;
    }

    public TrackPiece setLength(Double length) {
        this.length = length;
        return this;
    }

    public boolean getHasSwitch() {
        return hasSwitch;
    }

    public TrackPiece setHasSwitch(boolean hasSwitch) {
        this.hasSwitch = hasSwitch;
        return this;
    }

    public Double getRadius() {
        return radius;
    }

    public TrackPiece setRadius(Double radius) {
        this.radius = radius;
        return this;
    }

    public Double getAngle() {
        return angle;
    }

    public TrackPiece setAngle(Double angle) {
        this.angle = angle;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(length, hasSwitch, radius, angle);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TrackPiece) {
            TrackPiece other = (TrackPiece) obj;
            return equal(length, other.length)
                    && equal(hasSwitch, other.hasSwitch)
                    && equal(radius, other.radius)
                    && equal(angle, other.angle);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("length", length)
                .add("hasSwitch", hasSwitch)
                .add("radius", radius)
                .add("angle", angle)
                .toString();
    }
}
