package org.moonlightflame.hwo.msg;


/**
 * @author aectann
 */
public enum MessageType {

    JOIN("join"),
    YOUR_CAR("yourCar"),
    GAME_INIT("gameInit"),
    GAME_START("gameStart"),
    CAR_POSITIONS("carPositions"),
    THROTTLE("throttle"),
    SWITCH_LANE("switchLane"),
    CRASH("crash"),
    SPAWN("spawn"),
    LAP_FINISHED("lapFinished"),
    FINISH("finish"),
    GAME_END("gameEnd"),
    TOURNAMENT_END("tournamentEnd"),
    DISQUALIFIED("dnf"),

    CREATE_RACE("createRace"),
    JOIN_RACE("joinRace"),

    PING("ping")
    ;

    private final String strRep;


    MessageType(String strRep) {
        this.strRep = strRep;
    }

    public String getStringRep() {
        return strRep;
    }

    public static MessageType forStrRep(String rep) {
        for (MessageType mt: values()) {
            if (mt.strRep.equalsIgnoreCase(rep)) {
                return mt;
            }
        }
        return null;
    }
}
