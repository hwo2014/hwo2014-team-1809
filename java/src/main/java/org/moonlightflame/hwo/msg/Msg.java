package org.moonlightflame.hwo.msg;


import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

import static com.google.common.base.Objects.equal;


/**
 * @author aectann
 */
public class Msg<T> {

    @SerializedName("msgType")
    protected MessageType type;

    protected String gameId;

    protected Integer gameTick;

    protected T data;

    public Msg() {
    }

    public Msg(MessageType type) {
        this.type = type;
    }

    public Msg(MessageType type, T data) {
        this.type = type;
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Integer getGameTick() {
        return gameTick;
    }

    public void setGameTick(Integer gameTick) {
        this.gameTick = gameTick;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(type, data);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Msg) {
            Msg other = (Msg) obj;
            return equal(type, other.type)
                    && equal(data, other.data);
        }
        return false;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("type", type)
                .add("data", data)
                .toString();
    }
}
