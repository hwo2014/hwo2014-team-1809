package org.moonlightflame.hwo.msg;


/**
 * @author aectann
 */
public class Ping extends SendMsg {

    @Override
    protected String msgType() {
        return "ping";
    }
}
