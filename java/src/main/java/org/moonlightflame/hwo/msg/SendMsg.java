package org.moonlightflame.hwo.msg;


import com.google.gson.Gson;


/**
 * @author aectann
 */
public abstract class SendMsg {

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();

}
