package org.moonlightflame.hwo.msg.incoming;


import org.moonlightflame.hwo.model.CarPosition;
import org.moonlightflame.hwo.msg.MessageType;
import org.moonlightflame.hwo.msg.Msg;

import java.util.List;


/**
 * @author aectann
 */
public class CarPositionsMsg extends Msg<List<CarPosition>> {

    public CarPositionsMsg() {
        super(MessageType.CAR_POSITIONS);
    }

    public CarPositionsMsg(List<CarPosition> data) {
        super(MessageType.CAR_POSITIONS, data);
    }
}
