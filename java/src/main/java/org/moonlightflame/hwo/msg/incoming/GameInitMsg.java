package org.moonlightflame.hwo.msg.incoming;


import org.moonlightflame.hwo.model.RaceData;
import org.moonlightflame.hwo.msg.MessageType;
import org.moonlightflame.hwo.msg.Msg;


/**
 * @author aectann
 */
public class GameInitMsg extends Msg<RaceData> {

    public GameInitMsg() {
        super(MessageType.GAME_INIT);
    }

    public GameInitMsg(RaceData data) {
        super(MessageType.GAME_INIT, data);
    }
}
