package org.moonlightflame.hwo.msg.incoming;


import org.moonlightflame.hwo.msg.MessageType;
import org.moonlightflame.hwo.msg.Msg;


/**
 * @author aectann
 */
public class GameStartMsg extends Msg<Void> {

    public GameStartMsg() {
        super(MessageType.GAME_START);
    }

}
