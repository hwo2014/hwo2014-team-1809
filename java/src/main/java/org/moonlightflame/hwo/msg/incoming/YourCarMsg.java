package org.moonlightflame.hwo.msg.incoming;


import org.moonlightflame.hwo.model.CarId;
import org.moonlightflame.hwo.msg.MessageType;
import org.moonlightflame.hwo.msg.Msg;


/**
 * @author aectann
 */
public class YourCarMsg extends Msg<CarId> {

    public YourCarMsg() {
        super(MessageType.YOUR_CAR);
    }

    public YourCarMsg(CarId data) {
        super(MessageType.YOUR_CAR, data);
    }


}
