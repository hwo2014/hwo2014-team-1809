package org.moonlightflame.hwo.remote;


import java.net.Socket;


/**
 * @author aectann
 */
public class SocketListener {

    private final Socket socket;

    public SocketListener(Socket socket) {
        this.socket = socket;
    }
}
