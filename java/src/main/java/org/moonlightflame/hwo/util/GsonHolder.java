package org.moonlightflame.hwo.util;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.moonlightflame.hwo.msg.MessageType;
import org.moonlightflame.hwo.util.adapter.MessageTypeDeserializer;
import org.moonlightflame.hwo.util.adapter.MessageTypeSerializer;


/**
 * @author aectann
 */
public class GsonHolder {

    public static Gson getGson() {
        return GsonInnerHolder.jsonConverter;
    }


    private static class GsonInnerHolder {

        private static final Gson jsonConverter = new GsonBuilder()
                .registerTypeAdapter(MessageType.class, new MessageTypeSerializer())
                .registerTypeAdapter(MessageType.class, new MessageTypeDeserializer())
                .create();

    }

}
