package org.moonlightflame.hwo.util.adapter;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.moonlightflame.hwo.msg.MessageType;

import java.lang.reflect.Type;


/**
 * @author aectann
 */
public class MessageTypeDeserializer implements JsonDeserializer<MessageType> {

    @Override
    public MessageType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return MessageType.forStrRep(json.getAsJsonPrimitive().getAsString());
    }
}
