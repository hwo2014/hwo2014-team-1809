package org.moonlightflame.hwo.util.adapter;


import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.moonlightflame.hwo.msg.MessageType;

import java.lang.reflect.Type;


/**
 * @author aectann
 */
public class MessageTypeSerializer implements JsonSerializer<MessageType> {

    @Override
    public JsonElement serialize(MessageType src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getStringRep());
    }
}
