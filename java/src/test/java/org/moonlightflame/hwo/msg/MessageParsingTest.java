package org.moonlightflame.hwo.msg;


import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.junit.Test;
import org.moonlightflame.hwo.model.*;
import org.moonlightflame.hwo.msg.incoming.CarPositionsMsg;
import org.moonlightflame.hwo.msg.incoming.GameInitMsg;
import org.moonlightflame.hwo.msg.incoming.GameStartMsg;
import org.moonlightflame.hwo.msg.incoming.YourCarMsg;
import org.moonlightflame.hwo.util.GsonHolder;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.moonlightflame.hwo.testhelp.TestUtil.resourceAsString;


/**
 * @author aectann
 */
public class MessageParsingTest {

    @Test
    public void testSendJoin() throws Exception {
        testOutgoingMsg("/join.json", MessageType.JOIN, new Join("Schumacher", "UEWJBVNHDS"));
    }

    @Test
    public void testReceiveYourCar() throws Exception {
        testIncomingMsg("/yourcar.json", new YourCarMsg(new CarId("Schumacher", "red")));
    }


    @Test
    public void testReceiveGameInitMsg() throws Exception {
        List<TrackPiece> pieces = newArrayList(
                new TrackPiece().setLength(100.0),
                new TrackPiece().setLength(100.0).setHasSwitch(true),
                new TrackPiece().setRadius(200.0).setAngle(22.5)
        );
        List<Lane> lanes = newArrayList(
                new Lane(0, -20d),
                new Lane(1, 0d),
                new Lane(2, 20d)
        );
        StartPosition startingPoint = new StartPosition()
                .setPosition(new Coordinates(-340.0, -96.0))
                .setAngle(90.0);

        Track track = new Track("indianapolis", "Indianapolis")
                .setPieces(pieces)
                .setLanes(lanes)
                .setStartingPoint(startingPoint);

        List<Car> cars = newArrayList(
                new Car(new CarId("Schumacher", "red"), new CarDimensions(40.0, 20.0, 10.0)),
                new Car(new CarId("Rosberg", "blue"), new CarDimensions(40.0, 20.0, 10.0))
        );

        RaceSession session = new RaceSession(3, 30000l, true);

        testIncomingMsg("/gameinit.json", new GameInitMsg(new RaceData(new Race(track, cars, session))));
    }

    @Test
    public void testReceiveGameStart() throws Exception {
        testIncomingMsg("/gamestart.json", new GameStartMsg());
    }

    @Test
    public void testReceiveCarPositions() throws Exception {
        CarPositionsMsg msg = new CarPositionsMsg(newArrayList(
                new CarPosition(new CarId("Schumacher", "red"), 0.0, new PiecePosition(0, 0.0, new LanePosition(0, 0), 0)),
                new CarPosition(new CarId("Rosberg", "blue"), 45.0, new PiecePosition(0, 20.0, new LanePosition(1, 1), 0))
        ));

        msg.setGameId("OIUHGERJWEOI");
        msg.setGameTick(0);

        testIncomingMsg("/carpositions.json", new CarPositionsMsg());
    }

    @Test
    public void testSendThrottleMsg() throws Exception {
        testOutgoingMsg("/throttle.json", MessageType.THROTTLE, 1.0);
    }

    private <T> void testOutgoingMsg(String referenceJsonDoc, MessageType type, T testedObjects) {
        assertThat(
                gson().toJson(new Msg<T>(type, testedObjects)),
                is(equalTo(resourceAsString(referenceJsonDoc)))
        );
    }

    private <T extends Msg> void testIncomingMsg(String referenceJsonDoc, T testedObjects) {
        Msg result = gson().fromJson(resourceAsString(referenceJsonDoc), testedObjects.getClass());

        assertThat(result.getType(), is(equalTo(testedObjects.getType())));
        assertThat(result.getData(), is(equalTo(testedObjects.getData())));
    }



    private Gson gson() {
        return GsonHolder.getGson();
    }
}
