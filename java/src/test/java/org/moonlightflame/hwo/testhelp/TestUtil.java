package org.moonlightflame.hwo.testhelp;


import org.apache.commons.io.IOUtils;

import java.io.IOException;


/**
 * @author aectann
 */
public final class TestUtil {

    public static String resourceAsString(String pathToResource) {
        try {
            return IOUtils.toString(TestUtil.class.getResource(pathToResource));
        } catch (IOException e) {
            e.printStackTrace(System.err);
            return "";
        }
    }


    private TestUtil() {
        throw new AssertionError();
    }

}
