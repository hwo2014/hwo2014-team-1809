package org.moonlightflame.hwo.util;


import com.google.common.base.Objects;
import com.google.gson.Gson;
import org.junit.Test;
import org.moonlightflame.hwo.msg.MessageType;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author aectann
 */
public class AdapterTest {

    @Test
    public void testMessageTypeAdapter() throws Exception {
        Gson converter = GsonHolder.getGson();
        for (MessageType msgType: MessageType.values()) {
            ClassWithMessageType parsed = converter.fromJson("{\"type\":\"" + msgType.getStringRep() + "\"}", ClassWithMessageType.class);
            assertThat(parsed.getType(), is(equalTo(msgType)));

            String serialized = converter.toJson(new ClassWithMessageType().setType(msgType));
            assertThat(serialized, is(equalTo("{\"type\":\"" + msgType.getStringRep() + "\"}")));
        }
    }


    public static class ClassWithMessageType {
        private MessageType type;

        public MessageType getType() {
            return type;
        }

        public ClassWithMessageType setType(MessageType type) {
            this.type = type;
            return this;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ClassWithMessageType) {
                ClassWithMessageType other = (ClassWithMessageType) obj;
                return Objects.equal(type, other.type);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(type);
        }
    }
}
